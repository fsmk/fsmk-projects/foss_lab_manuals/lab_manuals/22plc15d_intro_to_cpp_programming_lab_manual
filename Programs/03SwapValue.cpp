/***************************************************************************
*File			: 03SwapValue.cpp
*Description	: Program swap 2 values by writing a function that uses call by reference technique.
*Author			: Prabodh C P
*Compiler		: gcc compiler, Ubuntu 22.04
*Date			: 10 January 2023
***************************************************************************/
#include <iostream>
#include <iomanip>
using namespace std;

void fnSwap(int&, int&);
/***************************************************************************
*Function		: 	main
*Input parameters	:	no parameters
*RETURNS		:	0 on success
***************************************************************************/
int main() {
    int iNum1, iNum2;
    cout << "Enter the value of m and n : " ;
    cin >> iNum1 >> iNum2;
    cout << "Values before Swapping m = " << iNum1 << " and n = " << iNum2 << endl;
    fnSwap(iNum1, iNum2);
    cout << "Values after Swapping m = " << iNum1 << " and n = " << iNum2 << endl;
    return 0;
}
/***************************************************************************
*Function		: 	fnSwap
*Input parameters	:	two parameters passed by reference
*RETURNS		:	nothing
***************************************************************************/
void fnSwap(int &p, int &q)
{
    p = p ^ q;
    q = p ^ q;
    p = p ^ q;
}
